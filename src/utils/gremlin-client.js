import { createClient, makeTemplateTag } from 'gremlin';

export const client = createClient(8182, 'localhost');

const gremlin = makeTemplateTag(client);

export default gremlin;
