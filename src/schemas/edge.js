import mongoose from 'mongoose';

const Node = mongoose.model('Edge', {
  id_old: { type: String, index: true },
  id_new: { type: String, index: true },
  inV_old: { type: String, index: true },
  inV_new: { type: String, index: true },
  outV_old: { type: String, index: true },
  outV_new: { type: String, index: true },
  label: { type: String, index: true },
  data: { type: mongoose.Schema.Types.Mixed },
  exported: { type: Boolean, default: false },
  locked: { type: Boolean, default: false },
  error: { type: Boolean, default: false },
  errorMesssage: String,
});

Node.on('index', (err) => {
  if (err) {
    return console.warn('Warn error : Edge event index', err);
  }
  return console.log('Edges fully indexed');
});

export default Node;
