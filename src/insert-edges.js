import _ from 'lodash';
import moment from 'moment';

import gremlin from './utils/gremlin-client';
import { Edge } from './utils/mongo-client';

const EDGES_CHUNK = 1;
const EDGES_LIMIT = 100;
let NB_EDGES = 0;
const ERRORS = [];

const verbose = (...data) => {
  if (process.env.VERBOSE) {
    verbose(data.join(' '));
  }
};

const insertEdge = async (edgeData) => {
  try {
    const newId = await gremlin`
      edgeId = g.V(${edgeData.outV_new}).next().addEdge(${edgeData.label}, g.V(${edgeData.inV_new}).next()).id()

      edge = g.E(edgeId)

      ${Object.keys(edgeData.properties)}.each{
        propertyKey = it;
        propertyData = ${edgeData.properties}[it];


        propertyData.each{

          propertyType = it.type;
          propertyValue = it.value;

          propertyValue.each{
            val = it
            if (propertyType == 'point') {
              edge.property(
                propertyKey,
                Geoshape.point(val.coordinates[0], val.coordinates[1])
              )
            } else if (propertyType == 'circle') {
              edge.property(
                propertyKey,
                Geoshape.circle(val.coordinates[0], val.coordinates[1], val.radius)
              )
            } else {
              edge.property(propertyKey, val)
            }
          }
        }
      }

      edge
      edge.id()
    `;
    return { id_new: newId[0], id_old: edgeData.id_old };
  } catch (err) {
    console.error('ERROR =>', err);
    // process.exit();
    await Edge.update({ id_old: edgeData.id_old }, { error: true, errorMessage: err });
    ERRORS.push(err);
    return undefined;
  }
};

const getType = (value) => {
  if (typeof value === 'object') {
    if (Array.isArray(value)) {
      return 'array';
    } else if (value.type === 'Point') {
      return 'point';
    } else if (value.type === 'Circle') {
      return 'circle';
    }
    return 'undefined';
  } else if (isNaN(value)) {
    return 'string';
  }
  return 'number';
};

const lockEdges = async edgesIds => await Edge.updateMany({ _id: { $in: edgesIds } }, { locked: true });

const getUnexportedEdges = async (limit = 1000) => {
  if (process.env.SAVE_METHOD === 'mongodb') {
    const edges = await Edge.find({
      exported: null,
      error: null,
      locked: null,
      inV_new: { $exists: true },
      outV_new: { $exists: true },
    }).limit(limit);

    await lockEdges(edges.map(edge => edge._id));
    return edges.map((edge) => {
      const returnValue = {};
      returnValue.label = edge.label;
      returnValue.id_old = edge.id_old;
      returnValue.inV_new = edge.inV_new;
      returnValue.outV_new = edge.outV_new;
      returnValue.properties = {};
      if (!edge.data) {
        return returnValue;
      }
      Object.keys(edge.data).forEach((property) => {
        let value = edge.data[property];
        if (Array.isArray(value)) {
          value = value.map(prop => prop.value)
            .map(val => ({
              value: !Array.isArray(val) ? [val] : val,
              type: getType(val),
            }));
        } else {
          value = {
            value: !Array.isArray(value) ? [value] : value,
            type: getType(value),
          };
        }
        if (!Array.isArray(value)) {
          value = [value];
        }
        returnValue.properties[property] = value;
      });
      return returnValue;
    });
  }
};

const markUpdatedEdges = async ({ id_old, id_new }) => await Promise.all([
  Edge.update({ id_old }, { id_new, exported: true, $unset: { locked: true } }),
]);

const getAndStoreEdges = async (limit = 1000) => {
  const edges = await getUnexportedEdges(limit);
  const chunkedEdges = _.chunk(edges, EDGES_CHUNK);

  for (let i = 0; i < chunkedEdges.length; i++) {
    const chunk = chunkedEdges[i];
    await Promise.all(
      chunk.map(insertEdge),
    )
      .then(async edgesIds => await Promise.all(
        edgesIds
          .filter(ids => !!ids)
          .map(markUpdatedEdges),
      ));
    NB_EDGES += EDGES_CHUNK;
  }
  return edges.length;
};


const showAvancement = (begin) => {
  const duration = moment.duration(moment().diff(begin)).asSeconds();
  console.log('\x1Bc');
  console.log('---------------------------------');
  console.log('==> Running for :', duration, 'seconds');
  console.log('Edges :');
  console.log('\tsaved :', NB_EDGES);
  console.log('Metrics :');
  console.log('\tinsertions / sec :', Math.ceil((NB_EDGES / duration) * 100) / 100);
  console.log('\terrors :', ERRORS.length);
  return setTimeout(() => showAvancement(begin), 1000);
};

const startSavingNodes = async () => {
  showAvancement(moment());
  console.log('SAVING EDGES ...');
  while (await getAndStoreEdges(EDGES_LIMIT)) {
    // console.log('saving chunk');
  }
  console.log('NB_EDGES :', NB_EDGES);
  console.log('NB_ERRORS :', ERRORS.length);
  process.exit();
};

startSavingNodes();
