

import _ from 'lodash';
import fs from 'fs';
import mkdirp from 'mkdirp';
import moment from 'moment';
import { walk } from 'walk';

import gremlin from './utils/gremlin-client';
import { Node, Edge, Dictionnary } from './utils/mongo-client';

const DATA_FOLDER = process.env.DATA_FOLDER || 'data';
const EDGES_FOLDER = process.env.EDGES_FOLDER || 'edges';
const ENTRY_POINT_ID = process.env.ENTRY_POINT_ID ||
  [16625760, 311368, 462896, 237640, 1347640, 520280, 12464, 135296, 348336, 909368, 999480];
const MAX_CHUNK_SIZE = process.env.MAX_CHUNK_SIZE || 50;
const NODES_FOLDER = process.env.NODES_FOLDER || 'nodes';
const NODES_EXTRACT_CHUNK = process.env.NODES_EXTRACT_CHUNK || 1;
const SAVE_METHOD = process.env.SAVE_METHOD || 'file';
const IGNORE_EDGES = process.env.IGNORE_EDGES || ['wallElement'];

const FOLDERS = new Set();
const PENDING_EDGES = new Set();
const PENDING_NODES = new Set();
let TMP_SAVED_EDGES = [];
let SAVED_EDGES = new Set();
const SAVED_NODES = new Set();

let NB_FILES = 0;
const SAVE_POOL = [];
const EXTRACT_POOL = [];

const verbose = (...data) => {
  if (process.env.VERBOSE) {
    console.log(data.join(' '));
  }
};
const verboseError = (...data) => {
  if (process.env.VERBOSE) {
    console.error(data.join(' '));
  }
};

const getDbSavedNodes = async (limit = 1000, skip = 0) => {
  const nodes = await Node.find().skip(skip).limit(limit);
  if (nodes.length > 0) {
    nodes.forEach(node => SAVED_NODES.add(node.id_old));
    return getDbSavedNodes(limit, skip + limit);
  }
  return true;
};

const getFileSavedNodes = async () => new Promise((resolve, reject) => {
  const folderPath = `${DATA_FOLDER}/${NODES_FOLDER}`;
  const walker = walk(folderPath, { followLinks: false });

  walker.on('file', (root, stat, next) => {
    const name = stat.name.split('.');
    name.pop();
    SAVED_NODES.add(name.join(''));
    next();
  });

  walker.on('errors', () => {
    reject(true);
  });

  walker.on('end', () => {
    resolve(true);
  });
});

const getDbSavedEdges = async (limit = 1000, skip = 0, nbEdges) => {
  const edges = await Edge.find().skip(skip).limit(limit);
  if (edges.length > 0) {
    edges.forEach(edge => TMP_SAVED_EDGES.push(edge.id_old));
    // edges.forEach(edge => SAVED_EDGES.add(edge.id_old));
    console.log('Get saved edges', TMP_SAVED_EDGES.length, 'of', nbEdges);
    return getDbSavedEdges(limit, skip + limit, nbEdges);
  }
  SAVED_EDGES = new Set(TMP_SAVED_EDGES);
  TMP_SAVED_EDGES = [];
  return true;
};

const getFileSavedEdges = async () => new Promise((resolve, reject) => {
  const folderPath = `${DATA_FOLDER}/${EDGES_FOLDER}`;
  const walker = walk(folderPath, { followLinks: false });

  walker.on('file', (root, stat, next) => {
    const name = stat.name.split('.');
    name.pop();
    SAVED_EDGES.add(name.join(''));
    next();
  });

  walker.on('errors', () => {
    reject(true);
  });

  walker.on('end', () => {
    resolve(true);
  });
});

const initSavedEdges = async () => {
  if (SAVE_METHOD === 'mongodb') {
    const nbEdges = await Edge.count({});
    console.log('NB EDGES', nbEdges);
    return await getDbSavedEdges(10000, 0, nbEdges);
  } else if (SAVE_METHOD === 'file') {
    return await getFileSavedEdges();
  }
};

const initSavedNodes = async () => {
  if (SAVE_METHOD === 'mongodb') {
    return await getDbSavedNodes();
  } else if (SAVE_METHOD === 'file') {
    return await getFileSavedNodes();
  }
};

const initPendingNodes = async () => {
  if (SAVE_METHOD === 'mongodb') {
    const nodes = await Dictionnary.find({ type: 'pending_nodes' });
    if (nodes && nodes[0] && nodes[0].data) {
      nodes[0].data.forEach(id => PENDING_NODES.add(id));
    }
  } else if (SAVE_METHOD === 'file') {
    let data = await fs.readFileSync('data/dictionnaries/pending_nodes.json', 'utf8');
    data = JSON.parse(data);
    data.forEach(id => PENDING_NODES.add(id));
  }
};

const initPendingEdges = async () => {
  if (SAVE_METHOD === 'mongodb') {
    const edges = await Dictionnary.find({ type: 'pending_edges' });
    if (edges && edges[0] && edges[0].data) {
      edges[0].data.forEach(id => PENDING_EDGES.add(id));
    }
  } else if (SAVE_METHOD === 'file') {
    let data = await fs.readFileSync('data/dictionnaries/pending_edges.json', 'utf8');
    data = JSON.parse(data);
    data.forEach(id => PENDING_EDGES.add(id));
  }
};

const getNodeData = async (id) => {
  if (id) {
    return await gremlin`
      g.V(${id})
    `;
  }
  return [];
};

const getEdgeData = async (ids) => {
  if (!Array.isArray(ids)) {
    ids = [ids];
  }
  if (!ids) {
    return Promise.resolve([]);
  }
  const fullData = [];
  const chunkIds = _.chunk(ids, MAX_CHUNK_SIZE);

  const fn = async (edgesIds) => {
    const chunkData = await gremlin`
      g.E(${edgesIds}.toArray())
        .has(label, without(${IGNORE_EDGES}))
    `;
    fullData.push(...chunkData);
  };

  for (let i = 0; i < chunkIds.length; i++) {
    const tempIds = chunkIds[i];
    try {
      await fn(tempIds);
    } catch (err) {
      console.log('ERROR', err);
      prepareExit();
    }
  }
  verbose('edges loaded', fullData.length);
  return fullData;
};

const getNodeEdgesIds = async (edgeId) => {
  if (edgeId) {
    return await gremlin`
      g.V(${edgeId})
        .bothE()
        .has(label, without(${IGNORE_EDGES}))
        .id()
    `;
  }
  return [];
};

// const saveSavedNodes = () => Dictionnary.update({ type: 'saved_nodes' }, { data: SAVED_NODES.values() });
// const saveSavedEdges = () => Dictionnary.update({ type: 'saved_edges' }, { data: SAVED_EDGES.values() });
const savePendingNodes = async () => {
  console.log('SAVING PENDING NODES :', SAVE_METHOD);
  if (SAVE_METHOD === 'mongodb') {
    await Dictionnary.update({ type: 'pending_nodes' }, { data: Array.from(PENDING_NODES) }, { upsert: true });
  } else if (SAVE_METHOD === 'file') {
    await saveDictionnaryToFile('pending_nodes', Array.from(PENDING_NODES));
  }
};
const savePendingEdges = async () => {
  console.log('SAVING PENDING EDGES :', SAVE_METHOD);
  if (SAVE_METHOD === 'mongodb') {
    await Dictionnary.update({ type: 'pending_edges' }, { data: Array.from(PENDING_EDGES) }, { upsert: true });
  } else if (SAVE_METHOD === 'file') {
    await saveDictionnaryToFile('pending_edges', Array.from(PENDING_EDGES));
  }
};

const prepareExit = () => {
  Promise.all([
    savePendingNodes(),
    savePendingEdges(),
  ])
    .then(() => process.exit(1));
};

const checkFolder = async (folderPath) => {
  if (!FOLDERS.has(folderPath)) {
    return new Promise((resolve, reject) => mkdirp(folderPath, (err) => {
      if (err) {
        return reject(err);
      }
      return resolve(true);
    }));
  }
  return Promise.resolve(folderPath);
};

const saveDictionnaryToFile = async (type, data) => {
  const folderPath = `${DATA_FOLDER}/dictionnaries`;
  const fileName = `${folderPath}/${type}.json`;

  await checkFolder(folderPath);

  if (fs.existsSync(fileName)) {
    return Promise.resolve(false);
  }

  data = JSON.stringify(data);
  return new Promise((resolve, reject) =>
    fs.writeFile(fileName, data, { flag: 'w' }, (err) => {
      if (err) {
        verboseError(err);
        return reject(err);
      }
      verbose(`dictionnary ${type} saved !`);
      return resolve(true);
    }),
  );
};

const saveNodeToFile = async (nodeData) => {
  const folderPath = `${DATA_FOLDER}/${NODES_FOLDER}/${nodeData.label}`;
  const fileName = `${folderPath}/${nodeData.id}.json`;


  await checkFolder(folderPath);

  if (fs.existsSync(fileName)) {
    return Promise.resolve(false);
  }

  const data = JSON.stringify(nodeData);
  return new Promise((resolve, reject) =>
    fs.writeFile(fileName, data, { flag: 'w' }, (err) => {
      if (err) {
        verboseError(err);
        return reject(err);
      }
      verbose(`node ${nodeData.id} saved ! (total : ${++NB_FILES} files)`);
      SAVED_NODES.add(nodeData.id);
      return resolve(true);
    }),
  );
};

const saveNodeToDb = async (nodeData) => {
  if (!nodeData) {
    PENDING_NODES.delete(nodeData);
    return;
  }
  const data = {
    id_old: nodeData.id,
    label: nodeData.label,
    data: nodeData.properties,
  };
  return Node.update({ id_old: nodeData.id }, data, { upsert: true })
    .then(() => {
      verbose(`node ${nodeData.id} saved ! (total : ${++NB_FILES})`);
      SAVED_NODES.add(nodeData.id);
      PENDING_NODES.delete(nodeData.id);
    });

  // const node = new Node(data);

  // return node.save()
  //   .then(() => {
  //     verbose(`node ${nodeData.id} saved ! (total : ${++NB_FILES})`);
  //     SAVED_EDGES.add(nodeData.id);
  //   });
};

const saveNode = async (nodeData) => {
  let savefunction = () => null;
  if (SAVE_METHOD === 'file') {
    savefunction = saveNodeToFile;
  } else if (SAVE_METHOD === 'mongodb') {
    savefunction = saveNodeToDb;
  }
  return savefunction(nodeData);
};

const saveEdgeToFile = async (edgeData) => {
  const folderPath = `${DATA_FOLDER}/${EDGES_FOLDER}/${edgeData.label}`;
  const fileName = `${folderPath}/${edgeData.id}.json`;

  await checkFolder(folderPath);

  if (fs.existsSync(fileName)) {
    return Promise.resolve(false);
  }

  const data = JSON.stringify(edgeData);

  return new Promise((resolve, reject) =>
    fs.writeFile(fileName, data, { flag: 'w' }, (err) => {
      if (err) {
        verboseError(err);
        return reject(err);
      }
      verbose(`edge ${edgeData.id} saved ! (total : ${++NB_FILES} files)`);
      SAVED_EDGES.add(edgeData.id);
      return resolve(true);
    }),
  );
};

const saveEdgeToDb = async (edgeData) => {
  const data = {
    id_old: edgeData.id,
    label: edgeData.label,
    inV_old: edgeData.inV,
    outV_old: edgeData.outV,
    data: edgeData.properties,
  };

  return Edge.update({ id_old: edgeData.id }, data, { upsert: true })
    .then(() => {
      verbose(`edge ${edgeData.id} saved ! (total : ${++NB_FILES})`);
      SAVED_EDGES.add(edgeData.id);
      PENDING_EDGES.delete(edgeData.id);
    });

  // const node = new Edge(data);
  // return node.save()
  //   .then(() => {
  //     verbose(`edge ${edgeData.id} saved ! (total : ${++NB_FILES})`);
  //     SAVED_EDGES.add(edgeData.id);
  //   });
};

const saveEdge = async (edgeData) => {
  let saveFunction = () => null;
  if (SAVE_METHOD === 'file') {
    saveFunction = saveEdgeToFile;
  } else if (SAVE_METHOD === 'mongodb') {
    saveFunction = saveEdgeToDb;
  }
  return saveFunction(edgeData);
};

const getAndSaveNode = async (id) => {
  const node = await getNodeData(id);
  await saveNode(node[0]);
};


const extract = async (id) => {
  verbose('#####################################');
  verbose('extract with id', id);
  if (!SAVED_NODES.has(id)) {
    PENDING_NODES.add(id);
    SAVE_POOL.push(() => getAndSaveNode(id));

    const edgesIds = await getNodeEdgesIds(id);
    const edgesToSave = [];

    for (let i = 0; i < edgesIds.length; i++) {
      const edgeId = edgesIds[i];
      if (
        !SAVED_EDGES.has(edgeId)
        && !PENDING_EDGES.has(edgeId)
      ) {
        PENDING_EDGES.add(edgeId);
        edgesToSave.push(edgeId);
      }
    }

    if (!edgesToSave.length) {
      return Promise.resolve(false);
    }

    const edgesData = await getEdgeData(edgesToSave);

    edgesData.forEach((edge) => {
      SAVE_POOL.push(() => saveEdge(edge));
    });

    edgesData.forEach((edge) => {
      // verbose('present', SAVED_NODES.indexOf(edge.inV) !== -1, 'id', id, 'inV', edge.inV);
      if (
        !SAVED_NODES.has(edge.inV)
        && !PENDING_NODES.has(edge.inV)
        && edge.inV !== id
      ) {
        PENDING_NODES.add(edge.inV);
        EXTRACT_POOL.push(() => extract(edge.inV));
      }
      // verbose('present', SAVED_NODES.indexOf(edge.outV) !== -1, 'id', id, 'outV', edge.outV);
      if (
        !SAVED_NODES.has(edge.outV)
        && !PENDING_NODES.has(edge.outV)
        && edge.outV !== id
      ) {
        PENDING_NODES.add(edge.outV);
        EXTRACT_POOL.push(() => extract(edge.outV));
      }
    });

    verbose('\tNB EDGES TOTAUX', edgesIds.length);
    verbose('\tNB EDGES', edgesToSave.length);
    verbose('\tPENDING_EDGES', PENDING_EDGES.size);
    verbose('\tPENDING_NODES', PENDING_NODES.size);
    verbose('\tSAVE_POOL', SAVE_POOL.length);
    verbose('\tEXTRACT_POOL', EXTRACT_POOL.length);
    verbose('');

    return Promise.resolve(true);
  }
  return Promise.resolve(false);
};

const executeSavePool = async () => {
  verbose('executeSavePool', SAVE_POOL.length);

  while (SAVE_POOL.length) {
    const action = SAVE_POOL.pop();
    await action();
  }
  verbose('save pool done');
};

const buildPools = async () => {
  Array.from(PENDING_NODES).forEach(id => EXTRACT_POOL.push(() => extract(id)));
  const edgesData = await getEdgeData(Array.from(PENDING_EDGES));

  edgesData.forEach((edge) => {
    SAVE_POOL.push(() => saveEdge(edge));
  });

  edgesData.forEach((edge) => {
    // verbose('present', SAVED_NODES.indexOf(edge.inV) !== -1, 'id', id, 'inV', edge.inV);
    if (
      !SAVED_NODES.has(edge.inV)
      && !PENDING_NODES.has(edge.inV)
    ) {
      PENDING_NODES.add(edge.inV);
      EXTRACT_POOL.push(() => extract(edge.inV));
    }
    // verbose('present', SAVED_NODES.indexOf(edge.outV) !== -1, 'id', id, 'outV', edge.outV);
    if (
      !SAVED_NODES.has(edge.outV)
      && !PENDING_NODES.has(edge.outV)
    ) {
      PENDING_NODES.add(edge.outV);
      EXTRACT_POOL.push(() => extract(edge.outV));
    }
  });
  console.log('SAVE_POOL size: ', SAVE_POOL.length);
  console.log('EXTRACT_POOL size: ', EXTRACT_POOL.length);
};

const executePools = async () => {
  while (SAVE_POOL.length || EXTRACT_POOL.length) {
    if (SAVE_POOL.length) {
      await executeSavePool();
    }
    if (EXTRACT_POOL.length) {
      for (let i = 0; i <= NODES_EXTRACT_CHUNK; i++) {
        const extractJob = EXTRACT_POOL.pop();
        if (extractJob) {
          await extractJob();
        }
      }
    }
  }
  if (!SAVE_POOL.length && !EXTRACT_POOL.length && PENDING_NODES.size) {
    const id = PENDING_NODES.values().next().value;
    EXTRACT_POOL.push(() => extract(id));
    return await executePools();
    // Array.from(PENDING_NODES).forEach(id => EXTRACT_POOL.push(() => extract(id)));
  }
  return 'done';
};

const showAvancement = (begin) => {
  const duration = moment.duration(moment().diff(begin)).asSeconds();
  console.log('\x1Bc');
  console.log('---------------------------------');
  console.log('==> Running for :', duration, 'seconds');
  console.log('Edges :');
  console.log('\tsaved :', SAVED_EDGES.size);
  console.log('\tpending :', PENDING_EDGES.size);
  console.log('\ttotal :', PENDING_EDGES.size + SAVED_EDGES.size);
  console.log('Nodes :');
  console.log('\tsaved :', SAVED_NODES.size);
  console.log('\tpending :', PENDING_NODES.size);
  console.log('\ttotal :', PENDING_NODES.size + SAVED_NODES.size);
  console.log('Metrics :');
  console.log('\tEdges insertions / sec :', Math.ceil((SAVED_EDGES.size / duration) * 100) / 100);
  console.log('\tNodes insertions / sec :', Math.ceil((SAVED_NODES.size / duration) * 100) / 100);
  console.log('\tTotal insertions / sec :', Math.ceil(((SAVED_NODES.size + SAVED_EDGES.size) / duration) * 100) / 100);
  return setTimeout(() => showAvancement(begin), 1000);
};

const start = async (id = ENTRY_POINT_ID) => {
  console.time('Extraction done in');
  if (Array.isArray(ENTRY_POINT_ID)) {
    ENTRY_POINT_ID.forEach(entryId => PENDING_NODES.add(entryId));
  } else {
    PENDING_NODES.add(id);
  }
  const firstId = PENDING_NODES.values().next().value;
  PENDING_NODES.delete(firstId);
  extract(firstId);
  if (!process.env.VERBOSE) {
    showAvancement(moment());
  }
  console.log('pool extraction', await executePools());
  console.timeEnd('Extraction done in');
  process.exit();
};

try {
  Promise.all([
    initSavedNodes(),
    initSavedEdges(),
    initPendingNodes(),
    initPendingEdges(),
  ])
    .then(() => {
      console.log('NODES RETRIEVED :', SAVED_NODES.size);
      console.log('EDGES RETRIEVED :', SAVED_EDGES.size);
      console.log('NODES PENDING :', PENDING_NODES.size);
      console.log('EDGES PENDING :', PENDING_EDGES.size);
    })
    .then(async () => await buildPools())
    .then(() => start());
} catch (err) {
  console.error(err);
  prepareExit();
}

process
  .on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
    prepareExit();
  })
  .on('uncaughtException', (err) => {
    console.error(err, 'Uncaught Exception thrown');
    prepareExit();
  });

