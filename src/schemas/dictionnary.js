import mongoose from 'mongoose';

const Dictionnary = mongoose.model('Dictionnary', {
  type: { type: String, index: true },
  data: [{ type: String }],
});

Dictionnary.on('index', (err) => {
  if (err) {
    return console.warn('Warn error : Dictionnary event index', err);
  }
  return console.log('Dictionnary fully indexed');
});

export default Dictionnary;
