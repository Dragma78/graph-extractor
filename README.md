# graph-extractor

[TinkerPop](http://tinkerpop.apache.org/) compliant graph database ([TitanDB](http://titan.thinkaurelius.com/), [JanusGraph](http://janusgraph.org/), etc...) data extraction / insertion tool.

## Data extraction (from db to local)

Several modes of extraction :
* To .json files
* To [mongodb](https://www.mongodb.com/) (fastest)

**Warning, exporting a graph with millions of elements can take a very long time (roughly 1000 elements / seconds)**

> The extraction process takes one (or multiple) entry point(s), and follows edges and nodes until all has been covered.
> **It is important to note that single nodes with no edge, will never be covered. These nodes must set as one of the entry points**

### Save data into file system

The data is stored by defaut on the `/data` folder on the projet root folder.

Basic commands :
* To run the extraction type : `npm run extract:file`.
* To run the extraction in verbose mode, type : `npm run extract:file:verbose`.
* You can delete the data folder by typing : `npm run delete-data`.

Each graph element (edge / vertex) will be stored in a `.json` file, regarding the following architecture :

```
/data
∟ /nodes
  ∟ /<vetex label>
    ∟ <id>.json # 2442.json
    ∟ ...
  ∟ /<vetex label>
    ∟ ...
∟ /edges
  ∟ /<edge label>
    ∟ <id>.json # 3jfwny-wfmr4-3c45-1kt0mo.json
    ∟ ...
  ∟ /<edge label>
    ∟ ...
```

**Be carefull, do not extract large graph with millions of edges / vertices, or your hard drive will suffer ! ( ⚆ _ ⚆ )**

### Save data into mongodb

The data will be stored in mongo db. We will be using [mongoose](https://www.npmjs.com/package/mongoose) module for this.

**MongoDB have to be installed first**

Basic commands :
* To run the extraction type : `npm run extract:mongo`.
* To run the extraction in verbose mode, type : `npm run extract:mongo:verbose`.

Each graph element (edge / vertex) will be stored in a `Edge` and `Node` [mongoose](https://www.npmjs.com/package/mongoose) schema.
* [Watch edge definition](src/schemas/edge.js)
* [Watch node definition](src/schemas/node.js)

### Error handling

If for any rason, the script should throw an exception, the current state is stored before the process exits. The extraction will continue at the same place the next time you start it.

**Rebuilding the previous state can take a certain time (minutes for millions of elements)**

## Data insertion (from local to db)

This part only concerns the insertion process, from previously extracted data, to a TinkerPop compliant graph database.

> Only the mongodb option is available now. Insertion from stored files will come later.

### Insert data from mongodb

The process needs to be the following :
1. Insert the nodes with `npm run insert:nodes:mongo`
2. Insert the edges with `npm run insert:edges:mongo`

It is **important** that the nodes **must** be inserted to the targeted graph database **before** the edges are.

> **Insertion process can be done with simultaneous threads** (roughly 600 insertions / sec with 4 threads running, 300 / sec with only 1 thread)

## Todo

* graph-insertion from files tool

---

MIT License

Copyright (c) 2018 Florent Béjina

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
