import _ from 'lodash';
import moment from 'moment';

import gremlin from './utils/gremlin-client';
import { Node, Edge } from './utils/mongo-client';

const NODES_CHUNK = 1;
const NODES_LIMIT = 100;
let NB_NODES = 0;
const ERRORS = [];

const verbose = (...data) => {
  if (process.env.VERBOSE) {
    verbose(data.join(' '));
  }
};

const insertNode = async (nodeData) => {
  try {
    const newId = await gremlin`
      node = g.addV(label, ${nodeData.label})

      ${Object.keys(nodeData.properties)}.each{
        propertyKey = it;
        propertyData = ${nodeData.properties}[it];


        propertyData.each{

          propertyType = it.type;
          propertyValue = it.value;

          propertyValue.each{
            val = it
            if (propertyType == 'point') {
              node.property(
                propertyKey,
                Geoshape.point(val.coordinates[0], val.coordinates[1])
              )
            } else if (propertyType == 'circle') {
              node.property(
                propertyKey,
                Geoshape.circle(val.coordinates[0], val.coordinates[1], val.radius)
              )
            } else {
              node.property(propertyKey, val)
            }
          }
        }
      }

      node
      node.id()
    `;
    return { id_new: newId[0], id_old: nodeData.id_old };
  } catch (err) {
    console.error('ERROR =>', err);
    // process.exit();
    await Node.update({ id_old: nodeData.id_old }, { error: true, errorMessage: err });
    ERRORS.push(err);
    return undefined;
  }
};

const getType = (value) => {
  if (typeof value === 'object') {
    if (Array.isArray(value)) {
      return 'array';
    } else if (value.type === 'Point') {
      return 'point';
    } else if (value.type === 'Circle') {
      return 'circle';
    }
    return 'undefined';
  } else if (isNaN(value)) {
    return 'string';
  }
  return 'number';
};

const lockNodes = async nodesIds => await Node.updateMany({ _id: { $in: nodesIds } }, { locked: true });

const getUnexportedNodes = async (limit = 1000) => {
  if (process.env.SAVE_METHOD === 'mongodb') {
    const nodes = await Node.find({ exported: null, error: null, locked: null }).limit(limit);
    await lockNodes(nodes.map(node => node._id));
    return nodes.map((node) => {
      const returnValue = {};
      returnValue.label = node.label;
      returnValue.id_old = node.id_old;
      returnValue.properties = {};
      if (!node.data) {
        return returnValue;
      }
      Object.keys(node.data).forEach((property) => {
        const value = node.data[property]
          .map(prop => prop.value)
          .map(val => ({
            value: !Array.isArray(val) ? [val] : val,
            type: getType(val),
          }));
        returnValue.properties[property] = value;
      });
      return returnValue;
    });
  }
};

const markUpdatedNode = async ({ id_old, id_new }) => await Promise.all([
  Node.update({ id_old }, { id_new, exported: true, $unset: { locked: true } }),
  Edge.updateMany({ inV_old: id_old }, { inV_new: id_new }),
  Edge.updateMany({ outV_old: id_old }, { outV_new: id_new }),
]);

const getAndStoreNodes = async (limit = 1000) => {
  const nodes = await getUnexportedNodes(limit);
  const chunkedNodes = _.chunk(nodes, NODES_CHUNK);

  for (let i = 0; i < chunkedNodes.length; i++) {
    const chunk = chunkedNodes[i];
    await Promise.all(
      chunk.map(insertNode),
    )
      .then(async nodeIds => await Promise.all(
        nodeIds
          .filter(ids => !!ids)
          .map(markUpdatedNode),
      ));
    NB_NODES += NODES_CHUNK;
  }
  return nodes.length;
};

const showAvancement = (begin) => {
  const duration = moment.duration(moment().diff(begin)).asSeconds();
  console.log('\x1Bc');
  console.log('---------------------------------');
  console.log('==> Running for :', duration, 'seconds');
  console.log('Nodes :');
  console.log('\tsaved :', NB_NODES);
  console.log('Metrics :');
  console.log('\tinsertions / sec :', Math.ceil((NB_NODES / duration) * 100) / 100);
  console.log('\terrors :', ERRORS.length);
  return setTimeout(() => showAvancement(begin), 1000);
};

const startSavingNodes = async () => {
  console.log('SAVING NODES ...');
  showAvancement(moment());
  while (await getAndStoreNodes(NODES_LIMIT)) {
    // console.log('saving chunk');
  }
  console.log('NB_NODES :', NB_NODES);
  console.log('NB_ERRORS :', ERRORS.length);
  process.exit();
};

startSavingNodes();
