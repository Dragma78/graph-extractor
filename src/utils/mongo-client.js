import mongoose from 'mongoose';

import Edge from '../schemas/edge';
import Node from '../schemas/node';
import Dictionnary from '../schemas/dictionnary';

const MONGODB_DB_NAME = process.env.MONGODB_DB_NAME || 'backup';
const MONGODB_URL = process.env.MONGODB_URL || 'mongodb://localhost:27017';
const SAVE_METHOD = process.env.SAVE_METHOD || 'file';

if (SAVE_METHOD === 'mongodb') {
  mongoose.connect(`${MONGODB_URL}/${MONGODB_DB_NAME}`, (err) => {
    if (err) { throw err; }
  });
}

export default mongoose;
export { Node, Edge, Dictionnary };
